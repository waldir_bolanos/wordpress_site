<?php
add_action( 'wp_enqueue_scripts', 'custom_load_font_awesome' );
/**
 * Enqueue Font Awesome.
 */
function custom_load_font_awesome() {

    wp_enqueue_style( 'font-awesome-free', '/wp-includes/vendor/font-awesome/css/all.css' );

}